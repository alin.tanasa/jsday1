var arr = [1, 2, 'hello', NaN, {
  city: 'IasI',
  zip: null
}, [11, 12], undefined, undefined, undefined];

function getStats(arr) {
  for (var i = 0; i < arr.length; i++) {
    console.log(typeof arr[i]);
  }
}

getStats(arr);

function addF(param) {
  return function (x) {
    return param + x;
  }
};
let add = addF(13);
console.log(add(10));
console.log(add(-5));

function Add(x, y) {

  return x + y;

};

function limit(fn, max) {

  return fn;
};

let limitAdd = limit(Add, 2);
console.log(limitAdd(3, 5));
console.log(limitAdd(11, 23));
console.log(limitAdd(2, 2));

function whatIsInAName(arr, e) {
  const Ekey = Object.keys(e);
  for (var i = 0; i < arr.length; i++) {
    var j = 0;
    var keys = Object.keys(arr[i]);
    for (let key of keys) {
      if (arr[i][key] == e[key]) {
        j++;
      }
    }
    if (j == Ekey.length) {
      console.log(arr[i])
    }
  }
};
whatIsInAName([{ first: "Romeo", last: "Montague" }, { first: "Mercutio", last: null }, { first: "Tybalt", last: "Capulet" }], { last: "Capulet" });
whatIsInAName([{ "apple": 1 }, { "apple": 1 }, { "apple": 1, "bat": 2 }], { "apple": 1 });
whatIsInAName([{ "a": 1, "b": 2, "c": 3 }], { "a": 1, "b": 9999, "c": 3 });
